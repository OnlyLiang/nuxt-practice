module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/recommended` or `plugin:vue/essential`
    'plugin:vue/strongly-recommended'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    'vue/html-indent': ['error', 2, {
      'alignAttributesVertically': false
    }],
    'vue/script-indent': ['error', 2, {
      'baseIndent': 1,
      'switchCase': 1
    }],
    'vue/attribute-hyphenation': ['error', 'never', {
      'ignore': ['offset-']
    }],
    'vue/html-closing-bracket-newline': ['error', {
      'singleline': 'never',
      'multiline': 'never'
    }],
    'vue/max-attributes-per-line': ['error', {
      'singleline': 5,
      'multiline': {
        'max': 5
      }
    }],
    'vue/order-in-components': ['error', {
      'order': [
        'el',
        'name',
        'parent',
        'functional',
        ['delimiters', 'comments'],
        ['components', 'directives', 'filters'],
        'extends',
        'mixins',
        'inheritAttrs',
        'model',
        ['props', 'propsData'],
        'data',
        'computed',
        'watch',
        'methods',
        'LIFECYCLE_HOOKS',
        ['template', 'render'],
        'renderError'
      ]
    }],
    // "vue/component-name-in-template-casing": ["error", "PascalCase", {
    //   "registeredComponentsOnly": true,
    //   "ignores": ["/^b-/", "/^md-/", "/^mdc-/"]
    // }],
    'key-spacing': [2, {
      'align':
          'value'
    }],
    'object-curly-spacing': [2, 'always', {
      'objectsInObjects':
          true
    }],
    'space-before-function-paren': [2, {
      'anonymous': 'never',
      'named': 'always'
    }],
    'sort-imports': ['error', {
      'ignoreCase': true,
      'memberSyntaxSortOrder': ['none', 'all', 'single', 'multiple']
    }]
  }
}
