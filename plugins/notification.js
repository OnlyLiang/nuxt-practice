import 'toastr/build/toastr.min.css'
import 'jquery'
import toastr from 'toastr' 
import Vue from 'vue'
import VueNotifications from 'vue-notifications'

function toast ({ title, message, type, timeout, cb }) {
  if (type === VueNotifications.types.warn) type = 'warning'
  return toastr[type](message, title, { timeOut: timeout })
}
const options = {
  success: toast,
  error:   toast,
  info:    toast,
  warn:    toast
}

// VueNotifications.install = function(Vue,options){
Vue.prototype.$notifications = VueNotifications 
// }

Vue.use(VueNotifications, options)