import axios from 'axios'
import Vuex from 'vuex';

const store = () =>
  new Vuex.Store({
    state: ()=> {
      return{
        products: [
          {
            name:    '人工智慧實務應用',
            context: '人工智慧時代來臨，必須學習的新技術輕鬆學會「深度學習」：先學Keras再學TensorFlow',
            price:   '380',
            number:  '7',
          },
          {
            name:    '資料結構--使用Python',
            context: '資料結構(Data Structures)是資訊學科中的核心課程之一，也是基礎和必修的科目。',
            price:   '260',
            number:  '5',
          },
          {
            name:    'ffective SQL 中文版',
            context: '與其瞎忙或四處尋找答案，請幫自己一個忙：直接買這本書吧！！！',
            price:   '320',
            number:  '9',
          },
          {
            name:    '設計模式與C#實踐',
            context: '這本書是《無瑕的程式碼》系列書的第三冊，也是《名家名著》系列書的第三冊。',
            price:   '280',
            number:  '3',
          },
        ],
        cities:       {},
        followCities: []
      }     
    },
    mutations: {
      getData (state,res){
        state.cities = res
        console.log(state.cities)
      },
      followCitiesSet (state,city){
        state.followCities.push(city)        
      },
      // changeCitiesSet (state,index){
      //   state.cities.data.splice(index,1)
      // },
      unFollowCitiesSet  (state,index){
        state.followCities.splice(index,1)
      },
      // pushCitiesSet (state,city){
      //   state.cities.data.push(city)
      // },
      // sortByAirSet (state){
      //   state.cities.data.sort(function(a, b) {
      //     return a.AQI - b.AQI;
      //   });
      // },
      // sortByBadAirSet (state){
      //   state.cities.data.sort(function(a, b) {
      //     return b.AQI - a.AQI;
      //   });
      // }
    },
    actions: {
      getData (context) {
        axios.get('http://opendata2.epa.gov.tw/AQI.json')
          .then((res)=>{
            context.commit('getData',res)
          })
        console.log('拿資料')
      },
    }
  })

export default store;